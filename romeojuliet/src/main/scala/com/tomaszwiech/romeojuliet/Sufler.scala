package com.tomaszwiech.romeojuliet

import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.Actor

object Sufler {
  def props(name: String) = {
    Props(new Sufler(name, Map[String, ActorRef]()))
  }
  case class DataToSave(data: (Option[ActorRef], List[(String, String)]))
  object DataToStart
  case class SuflerBook(data: (Option[ActorRef], List[(String, String)]))
}

class Sufler(name: String, var actorsMap: Map[String, ActorRef]) extends Actor {
  import Sufler._
  import Director.{ SetActorsMap }

  val noActors: Option[ActorRef] = None
  val noText: List[(String, String)] = Nil

  override def preStart = println("Hello I'm sufler")

  var suflerData: (Option[ActorRef], List[(String, String)]) = (noActors, noText)

  def updateBook(data: (Option[ActorRef], List[(String, String)])) = suflerData = data

  override def receive = {
    case DataToSave(data) =>
      updateBook(data)
    case DataToStart => sender ! SuflerBook(suflerData)
    case SetActorsMap(map) =>
      println("I set map of actprs")
      actorsMap = map
      println("my map is set")
  }
}
