package com.tomaszwiech.romeojuliet

import akka.actor.{Actor, ActorRef, Props}
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import scala.concurrent.duration._

object Director {
  def props(actors: Array[String], book: List[(String,String)], sufler: ActorRef) = Props(new Director(actors, book, sufler))

  class BreakDownException extends Exception

  object Initialize
  object PrepareYourself
  object TryToRepair
  case class SetActorsMap(map: Map[String, ActorRef])
  case class WrongPlaying(book: List[(String, String)])
}

class Director(actorsNames: Array[String], val book: List[(String, String)], sufler: ActorRef) extends Actor {
  import Director._
  import MyActor.{End, Go, Ready, StopForMoment}
  import Technician.{AllOk, SomethingWrong}
  import Sufler.{SuflerBook, DataToStart}

  /**
    * Variable to handle reference to actors,  the position where the play should be restarted in case of problems, amount of problems and amount of stoped actors.
    */
  var actorsMap: Map[String, ActorRef] = null
  var problems: Int = 0
  var stopedActors = 0

  override val supervisorStrategy =
    //10 crashes in one minute stop actors.
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
      case _: BreakDownException => Restart
      case _: Exception => Escalate
    }

  def findFirstFrazeAndActor(book: List[(String, String)]): ActorRef = {
    actorsMap(book.head._2)
  }

  /**
    * Creating actors map -> (name, actorRef) and start watching.
    */
  override def preStart() = {
    actorsMap = (for{actorName <- actorsNames} yield (actorName -> context.actorOf(MyActor.props(actorName).withDispatcher("prio-dispatcher"), actorName))).toMap
    for{(actorName, actorRef) <- actorsMap} context.watch(actorRef)
    println("sending map to sufler")
    sufler ! SetActorsMap(actorsMap)
  }

  override def receive() = {
    /**
      * Creating map of child actors and send first message which initiate the play.
      */
    case Initialize =>
      val actor = findFirstFrazeAndActor(book)
      actor ! Go(book.tail, actorsMap)

    /**
      * Information from actor that something was wrong with his role. Director sends message asking for repeat.
      */
    case WrongPlaying(book) =>
      println(s"We need repeat phrase ${book.head._1} one more time")
      sender ! Go(book, actorsMap)

    /**
      * Actors sends data to restart.
      */
    case SuflerBook((nextActorOpt, book)) =>
      nextActorOpt match {
        case None =>
          println("I can't solve that problem. There is no actor. We are terminaing theatre");
          Theatre.theatre.terminate()
        case Some(nextActor) =>
          println("Next actor could start the play!")
          nextActor ! Go(book, actorsMap)
      }

    /**
      * Information about if there is something wrong in one of technican branch. Director asks actors for stop and technician for solving the problem.
      */
    case SomethingWrong(desc, from) =>
      problems match {
        case 0 =>
          println(s"Hello Actors. Problem in $desc. Stop playing for a moment.")
          for((name, ref) <- actorsMap) ref  ! StopForMoment(new BreakDownException());
        case _ => println(s"We have one more problem in $desc")
      }
      stopedActors = actorsMap.size
      problems += 1
      from ! TryToRepair

    /**
      * Information from technician that it has solved problems. Director checks if there are some more problems and sends messages to all actors askig them to set isStop value as false and he sends message to proper actor asking him to restart.
      */
    case AllOk =>
      problems -= 1
      if(problems > 0) {
        println(s"We have still $problems to solve")
      }
      else {
        println("We are starting in a moment!")
        for((name, ref) <- actorsMap) ref  ! PrepareYourself
      }
    /**
      * Actor resend information to director that it's state had been changed as ready(isStop is false).
      */
    case Ready =>
      stopedActors -= 1
      stopedActors match {
        case n if n > 0 => println("We are waiting for all") 
        case 0 =>
          sufler ! DataToStart
      }
    /**
      * The end of the sory. Director terminates system.
      */
    case End => println("The end"); Theatre.theatre.terminate()
  }
}

