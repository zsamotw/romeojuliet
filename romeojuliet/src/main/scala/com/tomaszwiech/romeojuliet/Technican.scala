package com.tomaszwiech.romeojuliet

import akka.actor.{Actor, Props, ActorRef}

object Technician {
  def props(branch: String) = Props(new Technician(branch))

  case class Info(desc: String)
  case class SomethingWrong(desc: String, from: ActorRef)
  object AllOk
}

class Technician(val branch: String) extends Actor {
  import Technician ._
  import Director.{TryToRepair}

  override def receive() = {
    /**
      * Sending information to director about the fact that something wrong had occured in the theatre.
      */
    case Info(desc) =>
      println(s"Something wrong: $desc")
      Theatre.director ! SomethingWrong(s"Problen in branch: $branch with $desc", self)
    /**
      * Procedure of repairing a failure after director message.
      */
    case TryToRepair =>
      println(s"I'm trying to solve the problem in branch: $branch. It will take some time.")
      Thread.sleep(3000)
      println(s"I have solved the probleme in branch: $branch")
      sender ! AllOk
  }
}
