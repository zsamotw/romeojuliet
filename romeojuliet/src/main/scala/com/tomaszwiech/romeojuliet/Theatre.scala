package com.tomaszwiech.romeojuliet

import akka.actor.ActorSystem

/**
  * Actor system with actors: director, sufler, lightman, costiumer.
  */
object Theatre extends App {

  val theatre = ActorSystem("theatre")
  val lightman = theatre.actorOf(Technician.props("Lightening"), "Tadek")
  val costiumer = theatre.actorOf(Technician.props("costiums"), "Adam")
  val actors = Array("julia", "romeo")
  val book = List(
    ("Not important", "romeo"),
    ("1 I love you Julia", "julia"),
    ("2 I love you Romeo", "romeo"),
    ("3 I love you Julia", "julia"),
    ("4 I love you Romeo", "romeo"),
    ("5 I love you Julia", "julia"),
    ("6 I love you Romeo", "romeo"),
    ("7 I love you Julia", "julia"),
    ("8 I love you Romeo", "romeo"),
    ("9 I love you Julia", "julia"),
  )
  val sufler = theatre.actorOf(Sufler.props("Henryk"))
  val director = theatre.actorOf(Director.props(actors, book, sufler), "Roman_Polanski")

  director ! Director.Initialize
  Thread.sleep(5000)
  lightman ! Technician.Info("Light broken")
  Thread.sleep(2000)
  costiumer ! Technician.Info("Constiums broken")
}
