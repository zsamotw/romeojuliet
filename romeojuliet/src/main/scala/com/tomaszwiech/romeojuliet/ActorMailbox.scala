package com.tomaszwiech.romeojuliet

import akka.actor.ActorSystem
import akka.dispatch.PriorityGenerator
import akka.dispatch.UnboundedStablePriorityMailbox
import com.typesafe.config.Config
import com.tomaszwiech.romeojuliet.MyActor.StopForMoment
/**
  * set priority mailbox for actors with priority for messages about stopping.
  */
class ActorMailbox(settings: ActorSystem.Settings, config: Config) extends UnboundedStablePriorityMailbox(
  PriorityGenerator {
    case StopForMoment(ex) => 0
    case otherwise => 1
  }
)
