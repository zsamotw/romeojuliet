package com.tomaszwiech.romeojuliet

import akka.actor.{Actor, ActorRef, Props}

object MyActor {
  def props(name: String) = Props(new MyActor(name))

  /**
    * Object for creation random numbers for setting if actor's role is Ok.
    */
  val rand = new scala.util.Random

  var isStoped: Boolean = _

  object End
  object Ready
  case class Go(book: List[(String, String)], actorsMap: Map[String, ActorRef])
  case class PlaceToRestart(book: List[(String, String)])
  case class StopForMoment(ex: Exception)
}

class MyActor(val name: String) extends Actor {
  import MyActor._
  import Director.{PrepareYourself, WrongPlaying}
  import Sufler.{DataToSave}

  /**
    * Method to check if actor plays well.
    */
  def isEnoughGood: Boolean = {
    val r = rand.nextInt(2)
    if(r == 0) false else true
  }

  def findNextActor(book: List[(String, String)], actorsMap: Map[String, ActorRef]): ActorRef = {
    actorsMap(book.head._2)
  }

  override def preStart() = {
    println(s"Hello. I'm $name and ready for play.")
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    println(s" $name preRestart")
    super.preRestart(reason, message)
  }

  override def receive() = {
    /**
      * actor's main method. It checks if the actor plays well and it sends the proper message to other actor or director(if isStoped is true).
      */
    case Go(book, actorsMap) =>
      (isStoped, book) match {
        case (_, Nil) => Theatre.director ! End
        case (false, _) =>
          val(fraze, nexActorName) = book.head
          val nextActor = findNextActor(book, actorsMap)
          if(isEnoughGood) {
            println(s"$fraze")
            Theatre.sufler ! DataToSave((Some(nextActor), book.tail))
            Thread.sleep(1000)
            nextActor ! Go(book.tail, actorsMap)
          }
          else {
            println(s"uuuuuuuu")
            println("oops!not perfect!")
            Thread.sleep(1000)
            Theatre.director ! WrongPlaying(book)
          }
        case (true, _) =>
         Theatre.director ! PlaceToRestart(book)
      }

    /**
      * throwing exception by actor when there are some problems.
      */
    case StopForMoment(ex) =>
      isStoped = true
      println("I have throw exception!!!")
      throw ex

    /**
      * Actor changes isStoped value as "false".
      */
    case PrepareYourself =>
      isStoped = false
      println(s"I'm ready!")
      sender ! Ready
  }
}
