Theatre Model:

There are:
- director => he is parent of all actors. If there are some problems ask actors to stop. He orders to solve problems reported by technicians. He knows the script of play. He creates proper amount of actors and sends them the script. 
- actors => plays it's phrases from head of the script and sends script's tail to another actor. Sometimes they must reperat the phrase if is not played enough well. They throw exception when some problems occures.
- technicians => stuff for solving technical problems.
